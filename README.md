# README

<!-- links -->
[unifi-docker]: https://hub.docker.com/r/jacobalberty/unifi
[ubuntu-20]: https://cdimage.ubuntu.com/releases/20.04.5/release/

## Setup

### Pre-emptively update the system

```bash
sudo apt update
sudo apt upgrade -y
```

### Setup a static IP

First, install the package:

```bash
sudo apt install -y network-manager
```

Then use `sudo nmtui` to set a static IP on the device.

Then for good measure, set a static IP with the existing UniFi controller.

### Setup as the initial user

```bash
cd bin/
./setup_user.sh
```

Now `sudo reboot` and log into the Pi as the new user.  Use the `sudo deluser`
command to delete the original user.

### Setup as the new user

```bash
cd bin/
./setup_devtools.sh
./setup_wireless.sh
./setup_mongo.sh
./setup_unifi.sh
```

## Notes

### Docker vs Host

Originally this project was designed to be a Docker Compose project.  There's
two options when dealing with UniFi and Docker Compose.

The first is we can use the very populate Docker project:
[jacobalberty/unifi][unifi-docker].  However, I felt this added an extra layer
of unknown software into my network stack.  I'm not suggesting that project is
malicious, but it does appear to be a fairly large project when I only had a day
or two to get something running.  I just did not want to add that to my network.

The second problem is how complicated the `unifi` package is.  To get the
`unifi` package installed, there is a lot that is needed to run.  Too much to
really run cleanly in a Docker container.  For example, it needs a very specific
(and old as fuck!) version of MongoDB.  You can't just run Mongo as a separate
container/service.  The `unifi` package doesn't like that, it needs access to
the real `mongod` binary.  There's even more complexity, that essentially
requires someone to recreate [jacobalberty/unifi][unifi-docker].

In the end, I decided to run the UniFi service directly on the host.

### Ubuntu 20 vs 22

Once I made the choice to run the UniFi Controller directly on the host, I first
tried running on Ubuntu 22 LTS.  This was fraught with issues.

Firstly, UniFi requires MongoDB version <5.  This is beyond deprecated and
unsupported from MongoDB.  Trying to install old versions of Mongo was nothing
short of a pain in the ass.  MongoDB depends the old version of libssl 1.1, of
which Ubuntu 22 ships with libssl 3+.  I've tried configuring apt to download
from the Ubuntu 20 (focal) repository.  This seemed to install libssl1.1, but I
was unable to easily get any MongoDB binaries to run without immediately failing
with an `Illegal instruction (core dumped)` error.  Presumably, it was calling
into the newer libssl3.  I've tried downloading the prebuilt binaries and
install from the MongoDB apt repositories.  Nothing worked on Ubuntu 22.

In the end... I just had to install [Ubuntu 20.04.5 LTS][ubuntu-20].
