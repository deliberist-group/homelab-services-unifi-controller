# CONTRIBUTING

The easiest way to contribute changes to the project is to fork the GitLab
repository, make the changes local to your forked repo, then submit a Merge
Request through GitLab.

Although there is not formal coding standards used with the code, your Merge
Request is more likely to be successful if your changes are consistent with the
existing coding style.  This includes indentations, variable/method names, and
even the amount of comments and documentation.

Merge Requests are also more likely to be successful if the commits also include
new tests to ensure the new or fixed functionality actually works.  Though there
are _some_ use cases where more tests will not be necessary.  This will be
determined on a case-by-case basis.

When in doubt, create tests and add code comments, and add as much information
as you can to the Merge Request.  The main developer will respond as soon as
possible.
