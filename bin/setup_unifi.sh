#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

# https://help.ui.com/hc/en-us/sections/7895096582039-UniFi-Consoles
# https://help.ui.com/hc/en-us/articles/220066768-Updating-and-Installing-Self-Hosted-UniFi-Network-Servers-Linux-
# https://www.ui.com/download/releases/network-server
# https://help.ui.com/hc/en-us/articles/360008976393

# Download the needed system dependencies.
sudo apt update
sudo apt upgrade -y
sudo apt install -y         \
    apt-transport-https     \
    ca-certificates         \
    curl                    \
    gnupg                   \
    openjdk-17-jre-headless

# Setup apt/
echo 'deb [ arch=amd64 ] https://www.ui.com/downloads/unifi/debian stable ubiquiti' \
    | sudo tee /etc/apt/sources.list.d/100-ubnt-unifi.list
cd /etc/apt/trusted.gpg.d/
sudo curl -O https://dl.ui.com/unifi/unifi-repo.gpg

# Install the UniFi Network Server.
sudo apt update
sudo apt install -y \
    unifi

# Status that bitch.
sudo systemctl status unifi

# ==============================================================================
# Now you can go to https://ip:8443/ and use a backup of the old controller in
# in the web UI setup wizard.
# ==============================================================================
