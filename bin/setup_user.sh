#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

# TODO change this as necessary
new_user_name=frank

sudo adduser "${new_user_name}"
sudo usermod -aG sudo "${new_user_name}"
