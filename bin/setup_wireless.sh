#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

# Disable WiFI and Bluetooth through modprobe.
blacklist_file=/etc/modprobe.d/raspi-blacklist.conf
sudo touch "${blacklist_file}"
echo 'blacklist brcmfmac' | sudo tee -a "${blacklist_file}"
echo 'blacklist brcmutil' | sudo tee -a "${blacklist_file}"
echo 'blacklist hci_uart' | sudo tee -a "${blacklist_file}"
echo 'blacklist btbcm' | sudo tee -a "${blacklist_file}"
echo 'blacklist btintel' | sudo tee -a "${blacklist_file}"
echo 'blacklist rfcom' | sudo tee -a "${blacklist_file}"
echo 'blacklist btqca' | sudo tee -a "${blacklist_file}"
echo 'blacklist btsdio' | sudo tee -a "${blacklist_file}"
echo 'blacklist bluetooth' | sudo tee -a "${blacklist_file}"

# Disable WiFi and Bluetooth through rfkill.
sudo apt install -y \
    rfkill
sudo rfkill block bluetooth
sudo rfkill block wifi
