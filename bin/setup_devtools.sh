#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

sudo apt update
sudo apt upgrade -y
sudo apt install -y \
    git             \
    tree            \
    vim
