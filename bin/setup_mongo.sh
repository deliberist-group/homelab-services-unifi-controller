#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

# https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/
# https://stackoverflow.com/questions/68937131/illegal-instruction-core-dumped-mongodb-ubuntu-20-04-lts
# https://stackoverflow.com/questions/68392064/error-when-running-mongo-image-docker-entrypoint-sh-line-381/68394912#68394912

sudo apt update
sudo apt upgrade -y
sudo apt install -y \
    curl            \
    gnupg

# Configure apt so it can pull from the MongoDB repositories.
mongo_gpg_file=/usr/share/keyrings/mongodb-server-4.4.gpg
curl -fsSL https://pgp.mongodb.com/server-4.4.asc | \
   sudo gpg -o "${mongo_gpg_file}" --dearmor
echo "deb [ arch=arm64 signed-by=${mongo_gpg_file} ]" \
        "https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" \
    | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

# Install a specific version of MongoDB as anything newer will either fail with
# an "Illegal instruction" error (see Stack Overflow links above due to CPU
# hardware) or a newer version will cause problems for the UniFi package.
sudo apt update
mongo_version=4.4.15
sudo apt install -y                         \
    mongodb-org="${mongo_version}"          \
    mongodb-org-server="${mongo_version}"   \
    mongodb-org-shell="${mongo_version}"    \
    mongodb-org-mongos="${mongo_version}"   \
    mongodb-org-tools="${mongo_version}"

# Pin the MongoDB package so a new "apt update" won't pull down the latest
# MongoDB in this major version.
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
echo "mongodb-database-tools hold" | sudo dpkg --set-selections
echo "mongodb-org-database-tools-extra hold" | sudo dpkg --set-selections

# ==============================================================================
# Note:
#
# At this point the MongoDB package will install a new systemctl service.
# However it is not activated.  The UniFi does not use that service, it actually
# runs the MongoDB commands directly as needed.  ¯\_(ツ)_/¯
# ==============================================================================
